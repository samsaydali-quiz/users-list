import {User} from './user.class';

export class SingleUserResponse {
  // tslint:disable:variable-name
  public data: User;
}
