import {User} from './user.class';

export class AllUsersResponse {
  // tslint:disable:variable-name
  public page: number;
  public per_page: number;
  public total: number;
  public total_pages: number;
  public data: User[];
}
