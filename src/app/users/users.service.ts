import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {AllUsersResponse} from './domain/all-users-response.class';
import {SingleUserResponse} from './domain/single-user-response.class';
import {User} from './domain/user.class';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private readonly endpoint;

  public selectedUser: User;
  public users: User[];
  public ready = false;

  public page = 1;
  public last;

  private cachedPages = {};
  private cachedUsers = {};

  constructor(private http: HttpClient, private router: Router) {
    this.endpoint = environment.apiEndpoint + 'users';
    this.initUsers();
  }

  public searchUser(id) {
    if (this.cachedUsers.hasOwnProperty(id)) {
      this.selectedUser = this.cachedUsers[id];
      this.selectUser(this.selectedUser);
    } else {
      this.getUser(id).subscribe((u) => {
        if (u.data.hasOwnProperty('id')) {
          this.selectUser(u.data);
        }
      });
    }
  }

  public selectUser(user: User) {
    this.cachedUsers[user.id] = user;
    this.selectedUser = user;
    this.router.navigate(['/users/details']);
  }

  public getPage(page) {
    this.page = page;
    if (this.cachedPages.hasOwnProperty(page)) {
      this.users = this.cachedPages[page];
    } else {
      this.ready = false;
      this.getAllUsers();
    }
  }

  private getAllUsers() {
    this.http.get<AllUsersResponse>(this.endpoint + `?page=${this.page}`)
      .subscribe((u) => {
      this.users = u.data;
      this.ready = true;
      this.last = u.total_pages;
      this.cachedPages[this.page] = u.data;
    });
  }

  private initUsers() {
    this.getAllUsers();
  }

  private getUser(id): Observable<SingleUserResponse> {
    return this.http.get<SingleUserResponse>(this.endpoint + `/${id}`);
  }


}
