import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../domain/user.class';
import {UsersService} from '../users.service';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {

  @Input() single = false;
  @Input() user: User;

  // @Output() userSelected = new EventEmitter<User>();

  constructor(private usersService: UsersService) { }

  ngOnInit() {
  }

}
