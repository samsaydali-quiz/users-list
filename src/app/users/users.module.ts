import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UserCardComponent } from './user-card/user-card.component';
import {UiElementsModule} from '../ui-elements/ui-elements.module';
import { UserDetailsComponent } from './user-details/user-details.component';


@NgModule({
  declarations: [UsersComponent, UsersListComponent, UserCardComponent, UserDetailsComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    UiElementsModule
  ],
})
export class UsersModule { }
