import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CardComponent } from './card/card.component';
import { PaginationComponent } from './pagination/pagination.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
  CardComponent,
  PaginationComponent,
  ],
  exports: [
    CardComponent,
    PaginationComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
  ]
})
export class UiElementsModule { }
