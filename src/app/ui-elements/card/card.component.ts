import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input()  title: string;
  @Input()  header: string;
  @Input()  footer: string;

  @Input()  noPadding = false;

  constructor() { }

  ngOnInit() {
  }

}
