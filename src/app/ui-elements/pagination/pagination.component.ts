import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Input() page = 1;
  @Input() last = -1;
  @Output() clickedNext = new EventEmitter();
  @Output() clickedPrev = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  disableNext(): boolean {
    return this.last !== -1 && (this.page === this.last || this.page > this.last);
  }
  disablePrev(): boolean {
    return this.page === 1;
  }

  clickNext() {
    if (!this.disableNext()) {
      this.clickedNext.emit(++this.page);
    }
  }

  clickPrev() {
    if (!this.disablePrev()) {
      this.clickedPrev.emit(--this.page);
    }
  }

}
